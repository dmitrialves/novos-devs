package io.todoapp.todoapp.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@ToString
public class Task{
    @Id
    @GeneratedValue
    @Getter @Setter
    private Long id;
    @Getter @Setter @NotNull @NotBlank
    private String name;
    @Getter @Setter
    private Boolean isDone;
}