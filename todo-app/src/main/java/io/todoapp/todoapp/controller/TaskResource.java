package io.todoapp.todoapp.controller;

import io.todoapp.todoapp.models.Task;
import io.todoapp.todoapp.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class TaskResource {
    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("/tasks")                   //list tasks
    public List<Task> retrieveAllTasks() {
        return taskRepository.findAll();
    }

    @PostMapping("/tasks")                  //insert a new task
    public ResponseEntity<Task> createTask(@RequestBody Task task) {
        task.setIsDone(false);
        Task newTask = taskRepository.save(task);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(newTask.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/tasks/{id}")              //edit task, search by id
    public ResponseEntity<Task> updateTask(@RequestBody Task task, @PathVariable long id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            task.setId(id);
            taskRepository.save(task);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/tasks/done/{id}")         //set task as finished
    public ResponseEntity<Task> doneTask(@PathVariable long id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            Task task = taskOptional.get();
            task.setIsDone(true);
            taskRepository.save(task);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/tasks/undo/{id}")         //set task as not finished
    public ResponseEntity<Task> undoTask(@PathVariable long id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            Task task = taskOptional.get();
            task.setIsDone(false);
            taskRepository.save(task);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/tasks/{id}")           //delete a task
    public ResponseEntity<Task> deleteTask(@PathVariable long id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            Task task = taskOptional.get();
            taskRepository.delete(task);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
