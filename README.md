# Auspex
# Novos Devs

Se chegou até aqui é porque está querendo entrar para nosso time, certo? Aqui na Auspex trabalhamos diariamente tentando superar nossos objetivos e sempre entregar o melhor para nossos clientes.
Estamos sempre buscando novas tecnologias e soluções para aumentar nossa produtividade, então aprender coisas novas faz parte da nossa rotina.
Prezamos muito por um ambiente de trabalho leve e descontraído e sabemos que o elemento chave para isso é o comprometimento de cada um com suas responsabilidades e com o grupo.

E aí, se identificou com a gente? Então vamos lá, já temos o primeiro desafio!

# Para rodar o projeto
```
./gradlew bootRun
```